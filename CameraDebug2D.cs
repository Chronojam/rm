using Godot;

public class CameraDebug2D : Camera2D
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        GlobalPosition = Vector2.Zero;
    }

    const int MOVE_SPEED = 1000;

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (Input.IsActionPressed("ui_left")) {
            GlobalPosition += Vector2.Left * delta * MOVE_SPEED;
        }
        if (Input.IsActionPressed("ui_right")) {
            GlobalPosition += Vector2.Right * delta * MOVE_SPEED;
        }
        if (Input.IsActionPressed("ui_down")) {
            GlobalPosition += Vector2.Down * delta * MOVE_SPEED;
        }
        if (Input.IsActionPressed("ui_up")) {
            GlobalPosition += Vector2.Up * delta * MOVE_SPEED;
        }
    }
}
