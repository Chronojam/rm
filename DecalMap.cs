using Godot;

public class DecalMap : TileMap
{
    const int DECAL_ATLAS_INDEX = 0;
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        
    }

    private Vector2 VendingMachineTop = new Vector2(4, 1);
    private Vector2 VendingMachineBottom = new Vector2(4, 2);

    public void PlaceDecalPavementSide(int x, int y) {
        this.SetCell(x, y, DECAL_ATLAS_INDEX, false, false, false, VendingMachineBottom);
        this.SetCell(x, y - 1, DECAL_ATLAS_INDEX, false, false, false, VendingMachineTop);
    }
//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
