using Godot;
using Godot.Collections;
public enum RoadSection {
    Left,
    Right,
    Up,
    Crossroads,
}

public class ProceduralRoad : TileMap
{
    const int ROADS_ATLAS_INDEX = 0;
    const int XRRatio = 20;

    int TrackLength = 5000;
    
    DecalMap DecalMap;
    public void GenerateRoads() 
    {
        RandomNumberGenerator rnd = new RandomNumberGenerator();
        RoadSection Next = RoadSection.Up;
        Array<RoadSection> Path = new Array<RoadSection>();
        Curve2D Curve = new Curve2D();
        Dictionary<RoadSection, int> PossibleSections = new Dictionary<RoadSection, int>(){
            {RoadSection.Left, 0},
            {RoadSection.Right, 0},
            {RoadSection.Up, XRRatio},
            {RoadSection.Crossroads, -3}, 
        };

        (int x, int y) Position = (0, 0);

        var spos = MapToWorld(new Vector2(Position.x, Position.y));
        spos += new Vector2(64, 32);
        Curve.AddPoint(spos);

        while(TrackLength > 0) {
            Path.Add(Next);
            switch(Next) {
                case RoadSection.Up:
                    CreateRoadPieceUp(Position.x, Position.y);
                    // Decide if we can place a crossroads, or another XR
                    if (PossibleSections[RoadSection.Crossroads] <= 0) {
                        PossibleSections[RoadSection.Crossroads]++;
                        Position.y --;
                        Next = RoadSection.Up;
                    } else {
                        var XRChance = rnd.RandiRange(PossibleSections[RoadSection.Up], PossibleSections[RoadSection.Crossroads]);
                        if (XRChance <= PossibleSections[RoadSection.Crossroads]) {
                            // Place XR
                            Position.y = Position.y - 3;
                            Next = RoadSection.Crossroads;
                            break;
                        }
                        Position.y --;
                        Next = RoadSection.Up;
                        PossibleSections[RoadSection.Crossroads]++;
                    }
                    break;
                case RoadSection.Left:
                    // Decide if we can place a crossroads, or another XR
                    CreateRoadPieceSide(Position.x, Position.y);
                    if (PossibleSections[RoadSection.Crossroads] <= 0) {
                        PossibleSections[RoadSection.Crossroads]++;
                        Position.x--;
                        Next = RoadSection.Left;
                    } else {
                        var XRChance = rnd.RandiRange(PossibleSections[RoadSection.Left], PossibleSections[RoadSection.Crossroads]);
                        if (XRChance <= PossibleSections[RoadSection.Crossroads]) {
                            // Place XR
                            Position.x -= 4;
                            Next = RoadSection.Crossroads;
                            break;
                        }
                        Position.x--;
                        PossibleSections[RoadSection.Crossroads]++;
                        Next = RoadSection.Left;
                    }
                    break;
                case RoadSection.Right:
                    CreateRoadPieceSide(Position.x, Position.y);
                    // Decide if we can place a crossroads, or another XR
                    if (PossibleSections[RoadSection.Crossroads] <= 0) {
                        PossibleSections[RoadSection.Crossroads]++;
                        Position.x++;
                        Next = RoadSection.Right;
                    } else {
                        var XRChance = rnd.RandiRange(PossibleSections[RoadSection.Right], PossibleSections[RoadSection.Crossroads]);
                        if (XRChance <= PossibleSections[RoadSection.Crossroads]) {
                            // Place XR
                            Position.x++;
                            Next = RoadSection.Crossroads;
                            break;
                        }
                        Position.x++;
                        PossibleSections[RoadSection.Crossroads]++;
                        Next = RoadSection.Right;
                    }
                    break;
                case RoadSection.Crossroads:
                    CreateCrossRoads(Position.x, Position.y);

                    // Create Waypoints for later.
                    var pos = MapToWorld(new Vector2(Position.x, Position.y));
                    pos += new Vector2(64, 64);
                    Curve.AddPoint(pos);
    
                    PossibleSections.Clear();

                    var LeftChance = rnd.RandiRange(0, 10);
                    var RightChance = rnd.RandiRange(0, 10);
                    var UpChance = rnd.RandiRange(0, 10);

                    // Look back to the last XR, see what direction it took.
                    RoadSection LastDirection = RoadSection.Up;
                    for (int i = Path.Count - 2; i > 0; i--) {
                        if (Path[i] == RoadSection.Crossroads) {
                            LastDirection = Path[i+1];
                            // Never go back on ourselves.
                            switch (LastDirection) {
                                case RoadSection.Left:
                                    RightChance = 0;
                                    break;
                                case RoadSection.Right:
                                    LeftChance = 0;
                                    break;
                            }
                            break;
                        }
                    }

                    if (LeftChance > RightChance) {
                        if (LeftChance > UpChance) {
                            Position.x--;
                            PossibleSections[RoadSection.Left] = XRRatio;
                            PossibleSections[RoadSection.Crossroads] = -3;
                            Next = RoadSection.Left;
                        } else {
                            Position.y--;
                            PossibleSections[RoadSection.Up] = XRRatio;
                            PossibleSections[RoadSection.Crossroads] = -3;
                            Next = RoadSection.Up;
                        }
                    } else {
                        if (RightChance > UpChance) {
                            Position.x += 4;
                            PossibleSections[RoadSection.Right] = XRRatio;
                            PossibleSections[RoadSection.Crossroads] = -3;
                            Next = RoadSection.Right;                
                        } else {
                            Position.y--;
                            PossibleSections[RoadSection.Up] = XRRatio;
                            PossibleSections[RoadSection.Crossroads] = -3;
                            Next = RoadSection.Up;
                        }
                    }
                    break;
            }
        }
        GetNode<Path2D>("RoadPath").Curve = Curve;
    }

    private Vector2 PavementTile = new Vector2(7, 4);
    private Vector2 RoadPavementTop = new Vector2(1, 7);
    private Vector2 RoadPavementBottom = new Vector2(1, 8);

    private void CreateRoadPieceSide(int x, int y)  {
        // Build road downwards from start position

        DecalMap.PlaceDecalPavementSide(x, y);
        this.SetCell(x, y, ROADS_ATLAS_INDEX, false, false, false, PavementTile);
        this.SetCell(x, y + 1, ROADS_ATLAS_INDEX, false, false, false, RoadPavementTop);
        this.SetCell(x, y + 2, ROADS_ATLAS_INDEX, false, false, false, RoadPavementBottom);
        this.SetCell(x, y + 3, ROADS_ATLAS_INDEX, false, false, false, PavementTile);
        TrackLength--;
    }

    private Vector2 RoadPavementLeft = new Vector2(4, 4);
    private Vector2 PavementCurbLeft = new Vector2(4, 5);
    private Vector2 RoadPavementRight = new Vector2(6, 5);
    private Vector2 PavementCurbRight = new Vector2(7, 5);
    
    private void CreateRoadPieceUp(int x, int y) {
        // Build road upwards from start position
        this.SetCell(x, y, ROADS_ATLAS_INDEX, false, false, false, PavementCurbLeft);
        this.SetCell(x + 1, y, ROADS_ATLAS_INDEX, false, false, false, RoadPavementLeft);
        this.SetCell(x + 2, y, ROADS_ATLAS_INDEX, false, false, false, RoadPavementRight);
        this.SetCell(x + 3, y, ROADS_ATLAS_INDEX, false, false, false, PavementCurbRight);
        TrackLength--;
    }
    private void CreateCrossRoads(int x, int y) {
        var XRstart = new Vector2(4,6);
        // Generate a crossroads
        for (int yc = 0; yc < 4; yc ++) {
            for (int xc = 0; xc < 4; xc ++) {
                this.SetCell(x + xc, y + yc, ROADS_ATLAS_INDEX, false, false, false, XRstart + new Vector2(xc, yc));
            }
        }

        this.SetCell(x + 1, y + 3, ROADS_ATLAS_INDEX, false, false, false, XRstart + new Vector2(1, 0));
        this.SetCell(x + 2, y + 3, ROADS_ATLAS_INDEX, false, false, false, XRstart + new Vector2(2, 0));
        TrackLength -= 4;
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
  public override void _Ready() 
  {
      DecalMap = GetNode<DecalMap>("DecalMap");
  }

  public override void _Process(float delta)
  {     

  }
}
