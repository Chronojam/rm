using Godot;
public class RootStreets : Node2D
{
    public override void _Ready()
    {
        // Stitch everything together
        var c = ResourceLoader.Load<PackedScene>("res://Car.tscn");
        var car = c.Instance<PathFollow2D>();
        var camera = new Camera2D();
        camera.Current = true;
        camera.Zoom = new Vector2(0.5f, 0.5f);
        car.AddChild(camera);

        var m = GetNode<ProceduralRoad>("Roads");
        m.GenerateRoads();
        m.GetNode("RoadPath").AddChild(car);
    }
}
